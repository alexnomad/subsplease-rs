use std::fs::{read, File};
use anyhow::bail;
use serde::{Serialize, Deserialize};

#[derive(Deserialize, Serialize, Debug)]
pub struct SavedTitle {
    pub page: String,
    pub last_episode: usize,
    pub saved_res: String,
}

#[derive(Debug)]
pub struct DataManager {
    saved_titles: Vec<SavedTitle>,
}

impl DataManager {
    pub fn init() -> anyhow::Result<Self> {
        Ok(Self { saved_titles: Self::load()? })
    }

    fn load() -> anyhow::Result<Vec<SavedTitle>> {
        let xdg_dirs = xdg::BaseDirectories::with_prefix("subsplease")?;
        let list_path = xdg_dirs.place_data_file("list.json")?;
        if list_path.exists() {
            let contents = String::from_utf8(read(list_path)?)?;
            let data: Vec<SavedTitle> = serde_json::from_str(&contents)?;
            Ok(data)
        } else {
            let list_file = File::create(list_path)?;
            let data: Vec<SavedTitle> = Vec::new();
            serde_json::to_writer_pretty(list_file, &data)?;
            Ok(data)
        }
    }

    fn save(&self) -> anyhow::Result<()>{
        let xdg_dirs = xdg::BaseDirectories::with_prefix("subsplease")?;
        let list_path = xdg_dirs.place_data_file("list.json")?;
        let list_file = File::create(list_path)?;
        serde_json::to_writer(list_file, &self.saved_titles)?;
        Ok(())
    }

    pub fn add(&mut self, data: SavedTitle) -> anyhow::Result<()> {
        if let Some(i) = self.saved_titles.iter().position(|t| t.page == data.page) {
            self.saved_titles[i] = data;
            self.save()?;
        } else {
            self.saved_titles.push(data);
            self.save()?;
        }
        Ok(())
    }

    // pub fn delete(&mut self, page: String) -> anyhow::Result<()> {
    //     if let Some(i) = self.saved_titles.iter().position(|t| t.page == page) {
    //         self.saved_titles.remove(i);
    //         return Ok(());
    //     }
    //     bail!("no title with page {} saved", page);
    // }

    pub fn load_saved_title(&mut self, page: String) -> anyhow::Result<&SavedTitle> {
        if let Some(i) = self.saved_titles.iter().position(|t| t.page == page) {
            self.saved_titles[i].last_episode += 1;
            return Ok(&self.saved_titles[i]);
        }
        bail!("{} has not been saved", page);
    }

    pub fn saved_titles(&self) -> &Vec<SavedTitle> {
        &self.saved_titles
    }
}
