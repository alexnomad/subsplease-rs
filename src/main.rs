mod search;
mod config;

use std::io;
use anyhow::bail;
use clap::Parser;
use config::{DataManager, SavedTitle};

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let mut args = Args::parse();
    let mut conf = DataManager::init()?;

    if args.generate_playlist {
        if let Some(q) = &args.search_query {
            let Some(res) = &args.res else {
                bail!("resolution isn't specified! Use --res")
            };
            let search_results = search::search(&q).await?;
            let mut episodes: Vec<&search::EpisodeInfo> = Vec::from_iter(search_results.values());
            // Sorting by episode
            episodes.sort_by(|a, b| a.episode.parse::<u32>().unwrap().partial_cmp(&b.episode.parse::<u32>().unwrap()).unwrap());
            
            for ep in episodes {
                let Some(download_info) = ep.downloads.iter().find(|d| d.res.trim() == res.trim()) else {
                    bail!("Incorrect resolution");
                };
                println!("{}", download_info.magnet);
            }
        }
        return Ok(());
    }

    if args.list_saved {
        println!("{:#?}", conf.saved_titles());
        return Ok(());
    }

    if let Some(saved_search) = &args.saved {
        let saved_title = conf.load_saved_title(saved_search.trim().to_string())?;
        args.episode = Some(saved_title.last_episode);
        args.res = Some(saved_title.saved_res.clone());
        args.search_query = Some(saved_title.page.clone());
    }
    
    // Actual request is done here
    let search_results = match args.search_query {
        Some(q) => {
            search::search(&q).await?
        },
        None => {
            let mut buf = String::new();
            println!("Search:");
            io::stdin().read_line(&mut buf)?;
            search::search(&buf).await?
        }
    };
    
    let mut episodes: Vec<&search::EpisodeInfo> = Vec::from_iter(search_results.values());

    // Sorting by episode
    episodes.sort_by(|a, b| a.episode.parse::<u32>().unwrap().partial_cmp(&b.episode.parse::<u32>().unwrap()).unwrap());

    let episode: usize;
    let page: String;
    // Checking if an episode is provided as an argument
    let downloads = match args.episode {
        Some(ep) => {
            if episodes.len() < ep {
                bail!("Episode {} does not exist in {}", ep, episodes[0].page);
            }

            episode = ep;
            page = episodes[ep - 1].page.clone();

            &episodes[ep - 1].downloads
        },
        None => {
            pretty_print_results(&episodes);
            println!("Select result: (1-{})", episodes.len());
            let mut b = String::new();
            io::stdin().read_line(&mut b)?;

            episode = b.trim().parse::<usize>()?;

            if episodes.len() < episode {
                bail!("Episode {} does not exist in {}", episode, episodes[0].page);
            }

            page = episodes[episode - 1].page.clone();

            &episodes[episode - 1].downloads
        },
    };

    // Checking if resolution is provided as an argument
    let res = match args.res {
        Some(r) => r,
        None => {
            println!("Select resolution: {:?}", downloads.iter().map(|d| &d.res).collect::<Vec<&String>>());
            let mut buf = String::new();
            io::stdin().read_line(&mut buf)?;
            buf
        }
    };

    let magnet = downloads.iter().find(|d| d.res.trim() == res.trim());
    match magnet {
        Some(m) => println!("{}", m.magnet),
        None => bail!("Invalid resolution!"),
    }
    conf.add(SavedTitle { 
        page,
        last_episode: episode, 
        saved_res: res.trim().to_string()
    })?;
    Ok(())
}

#[derive(Debug, Parser)]
#[command(author = "Alexey Shutov", version = "0.3.1", about = "Get anime magnet links from the comfort of your terminal!", long_about = None)]
struct Args {
    search_query: Option<String>,
    #[arg(short, long)]
    res: Option<String>,
    #[arg(short, long)]
    episode: Option<usize>,
    #[arg(short, long)]
    saved: Option<String>,
    #[arg(long)]
    list_saved: bool,
    #[arg(long)]
    generate_playlist: bool,
}

fn pretty_print_results(episodes: &Vec<&search::EpisodeInfo>) {
    let mut i = 1;
    for episode in episodes {
        if i % 2 == 0 {
            println!("{}", console::style(
                format!("{i}: {} {}\nreleased: {}", episode.show, episode.episode, episode.release_date)
            ).color256(194));
        } else {
            println!("{}", console::style(
                format!("{i}: {} {}\nreleased: {}", episode.show, episode.episode, episode.release_date)
            ).color256(230));
        }
        i += 1;
    }
}
