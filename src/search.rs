use std::collections::HashMap;

use anyhow::bail;

// Most of values here are needed just to correctly parse JSON
#[derive(Debug, serde::Deserialize)]
pub struct EpisodeInfo {
    pub time: String,
    pub release_date: String,
    pub show: String,
    pub episode: String,
    pub downloads: Vec<DownloadInfo>,
    pub xdcc: String,
    pub image_url: String,
    pub page: String,
}

#[derive(Debug, serde::Deserialize)]
pub struct DownloadInfo {
    pub res: String,
    pub magnet: String,
}

pub async fn search(q: &String) -> anyhow::Result<HashMap<String, EpisodeInfo>> {
    let responce = match reqwest::get(format!("https://subsplease.org/api/?f=search&tz=Europe/Moscow&s={q}"))
        .await?.json::<HashMap<String, EpisodeInfo>>().await {
            Ok(b) => b,
            Err(why) => bail!("No results or request failure! Additional info: {why}")
        };
    Ok(responce)
}

