# subsplease-rs

## Usage

#### One-line mode

`subsplease "<search query>" --episode <number> --resolution <number>`

if search query or episode or resolution is unspecified the program will prompt user as it would in interactive mode

#### Generate Playlist
`subsplease --genereate-playlist "<search query>" --res <number>`

#### Interactive mode
`subsplease`

I also recomend you try [mpv-webtorrent-hook](https://github.com/noctuid/mpv-webtorrent-hook) as it provides convinient way to play magnet links directly in mpv

`mpv <magnet link>`

## Examples

#### Play the first episode of Chainsaw Man in MPV (requires mpv-webtorrent-hook)
`subsplease "chainsaw man" -e 1 -r 1080 | mpv --playlist=-`

#### Play next episode of chainsaw man
Program by default saves last played episode in a series in /home/$USER/.local/share/subsplease/list.json

`subsplease -s "chainsaw-man"`

this should play the next episode of chainsaw man if you played it normaly

Please note that this feature doesn't work correctly at times

#### Play the entirety of one piece 
`subsplease "one piece" --generate-playlist --res 1080 | mpv --playlist=-`

## Known Bugs

Issues with episode names containing non-number characters

## Building

### Pre-requirements
- cargo
- git

### Automatic Install
`./install.sh`

### Just Build

`git clone https://gitlab.com/alexnomad/subsplease-rs`

`cd subsplease-rs`

`cargo build --release`

